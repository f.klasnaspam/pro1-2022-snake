package cz.uhk.pro1.snake.gui;

import cz.uhk.pro1.snake.model.Board;
import cz.uhk.pro1.snake.model.Cell;
import cz.uhk.pro1.snake.model.Game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameCanvas extends JComponent implements KeyListener, Runnable {

    private Game game;

    public GameCanvas() {
        game = new Game();
        setPreferredSize(new Dimension(Board.COL_COUNT * Cell.CELL_SIZE, Board.ROW_COUNT * Cell.CELL_SIZE));
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setFocusable(true);
        addKeyListener(this);
        new Thread(this).start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        game.paint(g);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
                game.setDirection(Game.RIGHT);
                break;
            case KeyEvent.VK_LEFT:
                game.setDirection(Game.LEFT);
                break;
            case KeyEvent.VK_UP:
                game.setDirection(Game.UP);
                break;
            case KeyEvent.VK_DOWN:
                game.setDirection(Game.DOWN);
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void run() {
        while (!game.isGameOver()) {
            game.update();
            repaint();
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
//        JOptionPane.showMessageDialog(this.getParent(), "Hra skončila :-(");
        new GameOverDialog(getParent(), game.getScore()).setVisible(true);
    }

}
