package cz.uhk.pro1.snake.gui;

import cz.uhk.pro1.snake.services.ScoreWriter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameOverDialog extends JDialog {

    public GameOverDialog(Component parentComponent, int score) {
        setPreferredSize(new Dimension(300, 150));
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        add(Box.createVerticalGlue());
        JLabel lblGameOver = new JLabel("Hra skončila :-(");
        lblGameOver.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(lblGameOver);
        add(Box.createVerticalGlue());
        JLabel lblScore = new JLabel("Skóre: " + score);
        lblScore.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(lblScore);
        add(Box.createVerticalGlue());
        JButton btnSave = new JButton("Uložit skóre do souboru");
        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ScoreWriter().save(score);
            }
        });
        btnSave.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(btnSave);
        add(Box.createVerticalGlue());
        pack();
        setModalityType(JDialog.DEFAULT_MODALITY_TYPE);
        setLocationRelativeTo(parentComponent);
    }

}
