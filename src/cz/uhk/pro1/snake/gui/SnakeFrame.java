package cz.uhk.pro1.snake.gui;

import javax.swing.*;

public class SnakeFrame extends JFrame {

    public SnakeFrame() {
        add(new GameCanvas());
        pack();
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
