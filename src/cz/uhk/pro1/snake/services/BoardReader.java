package cz.uhk.pro1.snake.services;

import cz.uhk.pro1.snake.model.Board;
import cz.uhk.pro1.snake.model.Cell;
import cz.uhk.pro1.snake.model.CellType;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BoardReader {

    private static final String FILENAME = "snake_board.csv";

    public Cell[][] load() {
        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {
            Cell[][] board = new Cell[Board.ROW_COUNT][Board.COL_COUNT];
            for (int i = 0; i < Board.ROW_COUNT; i++) {
                String line = br.readLine();
                String[] parts = line.split(",");
                for (int j = 0; j < Board.COL_COUNT; j++) {
                    Cell cell = new Cell(i, j);
                    if (parts[j].equals("1")) {
                        cell.setType(CellType.OBSTACLE);
                    }
                    board[i][j] = cell;
                }
            }
            return board;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
            // pokud by data v souboru neměla správný formát
            return null;
        }
    }

}
