package cz.uhk.pro1.snake.services;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScoreWriter {

    private static final String FILENAME = "snake_score.txt";

    public void save(int score) {
        /* PrintWriter pw = null;
        try { // try-catch-finally
            pw = new PrintWriter(new FileWriter(FILENAME, true));
            Date date = new Date();
            String dateString = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(date);
            pw.println(dateString + "   " + score);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (pw != null) {
                pw.close();
            }
        } */

        try (PrintWriter pw = new PrintWriter(new FileWriter(FILENAME, true))) { // try-with-resources
            Date date = new Date();
            String dateString = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(date);
            pw.println(dateString + "   " + score);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
