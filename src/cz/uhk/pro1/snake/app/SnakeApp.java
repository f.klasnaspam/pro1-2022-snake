package cz.uhk.pro1.snake.app;

import cz.uhk.pro1.snake.gui.SnakeFrame;

import javax.swing.*;

public class SnakeApp {

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() { // anonymní vnitřní třída
            @Override
            public void run() {
                new SnakeFrame().setVisible(true);
            }
        });

    }

}
