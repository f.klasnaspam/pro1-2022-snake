package cz.uhk.pro1.snake.model;

import java.awt.*;

public class Game {

    public static final int RIGHT = 1, LEFT = -1, UP = 2, DOWN = -2;
    private Board board;
    private Snake snake;
    private int direction;
    private boolean gameOver;
    private int score;

    public Game() {
        board = new Board();
        snake = new Snake(board.getCell(0, 0));
        direction = RIGHT;
        gameOver = false;
        board.generateFood();
    }

    public void setDirection(int direction) {
        if (this.direction * -1 != direction) {
            this.direction = direction;
        }
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public int getScore() {
        return score;
    }

    public void update() {
        Cell nextCell = getNextCell(snake.getHead());
        if (nextCell == null || nextCell.getType() == CellType.SNAKE_NODE || nextCell.getType() == CellType.OBSTACLE) {
            gameOver = true;
        } else {
            if (nextCell.getType() == CellType.FOOD) {
                snake.grow(nextCell);
                score++;
                board.generateFood();
            } else {
                snake.move(nextCell);
            }
        }
    }

    private Cell getNextCell(Cell currentPosition) {
        int row = currentPosition.getRow();
        int column = currentPosition.getColumn();
        switch (direction) {
            case RIGHT:
                column++;
                break;
            case LEFT:
                column--;
                break;
            case UP:
                row--;
                break;
            case DOWN:
                row++;
                break;
        }
        if (row >= 0 && row < Board.ROW_COUNT && column >= 0 && column < Board.COL_COUNT) {
            return board.getCell(row, column);
        } else {
            return null;
        }
    }

    public void paint(Graphics g) {
        board.paint(g);
    }

}
