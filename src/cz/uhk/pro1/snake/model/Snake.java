package cz.uhk.pro1.snake.model;

import java.util.LinkedList;

public class Snake {

    private LinkedList<Cell> snakeParts = new LinkedList<>();

    public Snake(Cell init) {
        init.setType(CellType.SNAKE_NODE);
        snakeParts.add(init);
    }

    public void grow(Cell nextCell) {
        nextCell.setType(CellType.SNAKE_NODE);
        snakeParts.addFirst(nextCell);
    }

    public void move(Cell nextCell) {
        Cell tail = snakeParts.removeLast();
        tail.setType(CellType.EMPTY);
        nextCell.setType(CellType.SNAKE_NODE);
        snakeParts.addFirst(nextCell);
    }

    public Cell getHead() {
        return snakeParts.getFirst();
    }

}
