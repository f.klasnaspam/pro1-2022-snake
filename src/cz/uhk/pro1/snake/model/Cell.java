package cz.uhk.pro1.snake.model;

import java.awt.*;

public class Cell {

    public static final int CELL_SIZE = 20;
    private final int row;
    private final int column;
    private CellType type;

    public Cell(int row, int column) {
        this.row = row;
        this.column = column;
        this.type = CellType.EMPTY;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public CellType getType() {
        return type;
    }

    public void setType(CellType type) {
        this.type = type;
    }

    public void paint(Graphics g) {
        switch (type) {
            case SNAKE_NODE:
                g.setColor(Color.GREEN);
                g.fillRect(column * CELL_SIZE, row * CELL_SIZE, CELL_SIZE, CELL_SIZE);
                break;
            case FOOD:
                g.setColor(Color.RED);
                g.fillOval(column * CELL_SIZE, row * CELL_SIZE, CELL_SIZE, CELL_SIZE);
                break;
            case OBSTACLE:
                g.setColor(Color.BLACK);
                g.fillRect(column * CELL_SIZE, row * CELL_SIZE, CELL_SIZE, CELL_SIZE);
                break;
        }
    }

}
