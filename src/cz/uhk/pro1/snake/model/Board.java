package cz.uhk.pro1.snake.model;

import cz.uhk.pro1.snake.services.BoardReader;

import java.awt.*;
import java.util.Random;

public class Board {

    public static final int ROW_COUNT = 20;
    public static final int COL_COUNT = 20;
    private Cell[][] cells;

    public Board() {
        cells = new BoardReader().load();
        if (cells == null) {
            cells = new Cell[ROW_COUNT][COL_COUNT];
            for (int i = 0; i < ROW_COUNT; i++) {
                for (int j = 0; j < COL_COUNT; j++) {
                    cells[i][j] = new Cell(i, j);
                }
            }
        }
    }

    public Cell getCell(int row, int column) {
        return cells[row][column];
    }

    public void generateFood() {
        int i;
        int j;
        Random random = new Random();
        do {
            i = random.nextInt(ROW_COUNT);
            j = random.nextInt(COL_COUNT);
        } while (cells[i][j].getType() != CellType.EMPTY);
        cells[i][j].setType(CellType.FOOD);
    }

    public void paint(Graphics g) {
        for (int i = 0; i < ROW_COUNT; i++) {
            for (int j = 0; j < COL_COUNT; j++) {
                cells[i][j].paint(g);
            }
        }
    }

}
