package cz.uhk.pro1.snake.model;

public enum CellType {

    EMPTY,
    SNAKE_NODE,
    FOOD,
    OBSTACLE

}
